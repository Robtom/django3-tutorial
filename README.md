# Django3 Tutorial

django3-tutorial

Steps made.

Creating the project

`django-admin startproject mysite`

Creating app

`python3 manage.py startapp polls`

Running server

`python3 manage.py runserver 0:8000`

Migrating

`python manage.py migrate` 

Migration of app
Creating migration.
`python manage.py makemigrations polls`
Migrating the file.
`python manage.py sqlmigrate polls 0001`
`python manage.py migrate`

Checking project.
`python manage.py check`

Create super user
`python manage.py createsuperuser`

admin chilito